# pyTerminal

An application for accessing the terminal of a server using raw sockets.

A smart program for accessing other Linux computers via ssh.
Only available on Linux and other Unix based systems.

If there are any problems with the program, or if you find a bug or an error, or if you have any concerns or requests, feel free creating an issue concerning the problem or error.

## Dependencies

The following libraries and programs are required for running the terminal server and client

  * Python 2.7
  * Paramiko 1.14
  * PyCrypto 2.6.1
  * PyGithub
  * ecdsa

To install got into terminal and enter the following command

```
$ sudo pip install pycrypto paramiko ecdsa PyGithub
```

## Usage

#### SSH

An SSH key is required. To create one, enter the following command in terminal :

```
$ ssh-keygen
```

#### Instructions

For instructions enter the following commands into the command line
```
$ ./terminal.py
```

## Contributing

Anyone is allowed to contribute to this project. Fork the repo send pull requests to the master branch. The release branch is ony for the latest release products.