#!/usr/bin/python

#####################################################################
# pyTerminal                                                        #
# a smart program for accessing other linux computers via ssh       #
#                                                                   #
# requires pycrypto, paramiko and PyGithub                          #
# may require ecdsa                                                 #
#                                                                   #
# created by bendacoder                                             #
# github.com/bendacoder                                             #
#####################################################################

#make imports
from socket import *
from datetime import datetime
from pickle import load, dump
from subprocess import check_output as exeCMD
from sqlite3 import connect as SQLconnect
from Crypto.Hash import SHA256 as hasher#pip install pycrypto
from threading import Event
from urllib import urlopen as getPage
import os, sys
import paramiko#pip install paramiko

#help text
helpText = """>>>> P y T e r m i n a l <<<<
Created by Bendacoder
github.com/bendacoder

An application for accessing the terminal of a server.\nA smart way of accessing a linux computer.\nCreated by bendacoder\n
USAGE:\nsudo ./terminalServer option
option = createPswd or server or reset or client or viewData or sversion or update or help
\nif option is createPswd\n./terminal.py createPswd password2Hash
\nif option is client\n./terminal.py client serverIP serverPort serverUname serverPasswrod
\nif option is server\n./terminal.py server hostIP hostPort hostKeyPath hostKeyPswd=None logConnections=False
\nif option is reset\n./terminal.py reset
\nif option is viewData\n./terminal.py viewData
\nif option is sversion\n./terminal.py sversion
\nif option is update\n./terminal.py update
\nif option is help\n./terminal.py help
\nif option is removeResIP\n./terminal.py removeResIP anIPtoRemove
\nif option is addResIP\n./terminal.py addResIP anIPtoAdd
"""

#cmd help for client
cmdHelpText = """
PyTerminal : Command Line Interface
Special Commands : \n
quit OR Ctrl-C : Exit out of terminal
help : display help text

USE ANY LINUX COMMAND IN CMD LINE
"""

#important variables
importantVars = {
    "restIPFiles":"logs/server/restrictedIPs.dat",
    "connecLog":"logs/server/log.sqlite",
    "usrConnecLog":"logs/client/log.sqlite",
    "unames":["dacoder", "foobar"],
    "resCmds":["ssh-keygen", "sudo su"],
    "pswdFile":"usrData/pswd.dat",
    "version":"3.7.3"
    }

#check if logs dir exists
if not os.path.exists("logs"):
    os.mkdir("logs")
    os.mkdir("logs/server")
    os.mkdir("logs/client")

#check if input is ip
def checkIPv4(anInput):
    #seperate IP into 4 segments
    try:
        ipSegs = anInput.split(".")
    except:
        return(False)
    #check if this is ipv4
    if len(ipSegs) != 4:
        return(False)
    #check each part of ip
    for num in ipSegs:
        try:
            int(num)
        except:
            return(False)
    #if ip is totally normal
    return(True)

#functions required for commands
class cmdFunc:
    def __init__(self, resipfile, connlog, clientlog):
        self.connLog = connlog
        self.resIPfile = resipfile
        self.clientLog = clientlog
    #reset restricted ip
    def resResIp(self):
        #reset the file by dumping into it again
        with open(self.resIPfile, "wb") as resIPFile:
            dump([], resIPFile)
    #create and dump hash into password file
    def createStoreHash(self, aPaswd):
        #check if password dir exists
        if not os.path.exists("usrData"):
            os.mkdir("usrData")
        #check if password file exists
        if os.path.exists("usrData/pswd.txt"):
            os.remove(importantVars["pswdFile"])
        #create hash and hex it
        pwdHash = hasher.new(aPaswd).hexdigest()
        #dump hexdigest hash into auto created file
        dump(pwdHash, open(importantVars["pswdFile"], "wb"))
    #create table for log
    def createLogTable(self):
        #create database file
        with open(self.connLog, "w") as connecLog:
            connecLog.write("")
        #connect to database
        aDbTable = SQLconnect(self.connLog)
        #create table in database
        cur = aDbTable.cursor()
        try:
            cur.execute("CREATE TABLE connections (connDate text NOT NULL, ip text NOT NULL, authSucceed text NOT NULL)")
            cur.execute("CREATE TABLE cmds (cmdData text NOT NULL, ip text NOT NULL, cmd text NOT NULL, out text NOT NULL)")
        except:
            print("error : CANNOT CREATE TABLE IN LOG")
            sys.exit()
        #commit changes and close
        aDbTable.commit()
        aDbTable.close()
    #create client logs
    #can also be used to reset
    def createClientLog(self):
        with open(self.clientLog, "w") as clientLog:
            clientLog.write("")
        clientLogTable = SQLconnect(self.clientLog)
        cur = clientLogTable.cursor()
        cur.execute("CREATE TABLE connections (connDate text NOT NULL, serverIP text NOT NULL, authSucceed text NOT NULL)")
        clientLogTable.commit()
        clientLogTable.close()
    #view logs
    def viewSqliteDataLog(self):
        adb = SQLconnect(self.connLog)
        aCursor = adb.cursor()
        #get data from cursor given
        try:
            data = aCursor.execute("SELECT * FROM connections")
        except:
            print("error : CANNOT GET DATA FROM CURSOR GIVEN")
            sys.exit()
            #print data
        for item in data:
            print(item)
        #close db
        adb.close()
    #view commands issued
    def viewSqliteDataCmd(self):
        #connect to DB
        adb = SQLconnect(self.connLog)
        aCursor = adb.cursor()
        #get data
        try:
            data = aCursor.execute("SELECT * FROM cmds")
        except:
            print("error : CANNOT RETRIEVE DATA FROM CMDS LOGS")
            sys.exit()
        #display data
        for item in data:
            print(item)
    #view restricted ips
    def viewResIpData(self):
        #get and print data from file
        for item in load(open(self.resIPfile, "rb")):
            print(item)
    #view client data
    def viewClientData(self):
        adb = SQLconnect(self.clientLog)
        cur = adb.cursor()
        for line in cur.execute("SELECT * FROM connections"):
            print(line)
    #remove a restricted ip
    def removeAResIP(self, aResIP):
        if not checkIPv4(aResIP):
            print("error : PARAMETER " + str(aResIP) + " NOT IP FORMAT")
            sys.exit()
        #open file
        if not os.path.exists(self.resIPfile):
            print("error : CANNOT FIND FILE")
            sys.exit()
        resIPFiles = load(open(self.resIPfile, "rb"))
        #find ip
        if not aResIP in resIPFiles:
            print("error : CANNOT FIND IP IN RESTRICTED IP LIST FILE")
            sys.exit()
        #remove ip
        resIPFiles.remove(aResIP)
        #send to file
        dump(resIPFiles, open(self.resIPfile, "wb"))
    def addResIP(self, aResIP):
        if not checkIPv4(aResIP):
            print("error : PARAMETER " + str(aResIP) + "NOT IP FORMAT")
            sys.exit()
        if not os.path.exists(self.resIPfile):
            print("error : CANNOT FIND FILE")
            sys.exit()
        with open(self.resIPfile, "rb") as resIPfile:
            loadedIPlist = load(resIPfile)
        loadedIPlist.append(aResIP)
        with open(self.resIPfile, "wb") as resIPfileAsW:
            dump(loadedIPlist, resIPfileAsW)

#declare instance of cmdFunc with log and restricted IPS
cmdFunctions = cmdFunc(importantVars["restIPFiles"], importantVars["connecLog"], importantVars["usrConnecLog"])

#class for initiating server connection with client
class Server(paramiko.ServerInterface):
    #initialize object
    def __init__(self, AccUnames):
        self.event = Event()
        #get hashed stored password
        if not os.path.exists(importantVars["pswdFile"]):
            print("error : CANNOT FIND PASSWORD FILE\nRUN : ./terminal.py createPswd aPassword")
            sys.exit()
        #open password
        try:
            self.storredPswd = load(open(importantVars["pswdFile"], "rb"))#the client given password will be converted into hash
        except:
            print("error : CANNOT LOAD PASSWORD")
            sys.exit()
        if not isinstance(AccUnames, list):
            print("error : INVALID PARAMETER FOR LIST OF USER NAMES")
            sys.exit()
        #check if there are any entries in the list of unames
        if len(AccUnames) == 0:
            print("error : NO ENTRIES IN USER NAME LIST")
            sys.exit()
        self.uNames = AccUnames
    #check kind of channel request
    def check_channel_request(self, kind, chanid):
        if kind == "session":
            return paramiko.OPEN_SUCCEEDED
        return paramiko.OPEN_FAILED_ADMINISTRATIVELY_PROHIBITED
    #check password for user entry
    def check_auth_password(self, username, password):
        if (username in self.uNames) and (hasher.new(password).hexdigest() == self.storredPswd):
            return paramiko.AUTH_SUCCESSFUL
        return paramiko.AUTH_FAILED

#client terminal access function
def getAcTerm(serverIP, serverPort, serverUname, serverPassword, logConnec):
    #check if ip is fine for usage
    if not checkIPv4(serverIP):
        print("error : INVALID IP ADDRESS " + serverIP)
        sys.exit()
    #if the user wants to log connections
    if logConnec:
        #if log table does not exists
        if not os.path.exists(cmdFunctions.clientLog):
            cmdFunctions.createClientLog()
        #create instance of client table
        try:
            clientLogTable = SQLconnect(cmdFunctions.clientLog)
            cur = clientLogTable.cursor()
        except:
            print("error : CANNOT OPEN LOG TABLE")
            sys.exit()
    #create client instance
    clientSok = paramiko.SSHClient()
    clientSok.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #connect to server
    try:
        clientSok.connect(serverIP, serverPort, serverUname, serverPassword)
    except:
        print("error : CANNOT CONNECT TO SERVER; CHECK PARAMETERS GIVEN")
        #if the user wants to log connections
        if logConnec:
            cur.execute("""INSERT INTO connections (connDate, serverIP, authSucceed) VALUES (?, ?, ?)""", (datetime.now(), serverIP, "False"))
            clientLogTable.commit()
            clientLogTable.close()
        sys.exit()
    print("[+] connected to " + serverIP + ":" + str(serverPort))
    #create session
    try:
        session = clientSok.get_transport().open_session()
    except:
        print("error : CANNOT OPEN SESSION; AUTHENITCATION MUST HAVE FAILED")
        sys.exit()
    #begin loop for accepting commands for server
    print("[<=D] authentication succeeded! beginning terminal\n")
    #add data to log if needed
    if logConnec:
        cur.execute("""INSERT INTO connections (connDate, serverIP, authSucceed) VALUES (?, ?, ?)""", (datetime.now(), serverIP, "True"))
        clientLogTable.commit()
        clientLogTable.close()
    #accept commands while no keybreak
    try:
        while True:
            #get input from user
            cmd = raw_input(">> ")
            if cmd == "":
                continue
            #send command
            print("[<==] sending command to server")
            session.send(cmd.encode("utf-8"))
            #get output from server
            outs = session.recv(1000).decode("utf-8")
            print("[==>] received output")
            print(outs)
            #if command is quit
            if cmd == "quit":
                print("[-] exiting session")
                clientSok.close()
                break
    except KeyboardInterrupt:
        print("\n[-] exiting session")
        session.send("quit".encode("utf-8"))
        print(session.recv(1000).decode("utf-8"))
        clientSok.close()

#what to execute in command line
def terminal(hostIP, hostPort, hostKeyPath, hostKeyPswd, logConnections):#host key will be set to None type in main function
    #check ip
    if not checkIPv4(hostIP):
        print("error : INVALID IP " + hostIP)
        sys.exit()
    #get server key
    #check/find server key
    if not os.path.exists(hostKeyPath):
        print("error : CANNOT FIND HOST KEY IN PATH " + hostKeyPath)
        print("LAUNCH ssh-keygen TO GENERATE YOUR KEY")
        sys.exit()
    #get ssh key
    try:
        hostKey = paramiko.RSAKey(filename=hostKeyPath, password=hostKeyPswd)
    except:
        print("error : CANNOT CREATE HOST KEY OBJECT")
        sys.exit()
    #check for restricted ip file list
    if not os.path.exists(cmdFunctions.resIPfile):
        #create if does not exist
        print("[. . .] creating restricted ip list file")
        open(cmdFunctions.resIPfile, "w").close()
        #start creating structure
        cmdFunctions.resResIp()
        #message for creation
        print("[*] created " + cmdFunctions.resIPfile + "; do NOT edit (file structure may break)!\n")
    #check for log file
    if not os.path.exists(cmdFunctions.connLog):
        #create log file
        print("[. . .] creating log for connections")
        cmdFunctions.createLogTable()
        #message for creation
        print("[*] created " + cmdFunctions.connLog + "; do NOT edit (file structure may break)!\n")
    #if user wants to log connections
    if logConnections:
        aDBtable = SQLconnect(cmdFunctions.connLog)
    #create and configure socket
    sock = socket(AF_INET, SOCK_STREAM)
    sock.setsockopt(SOL_SOCKET, SO_REUSEADDR, 1)
    #bind to ip
    while True:
        #try with one address
        try:
            sock.bind((hostIP, hostPort))
            break
        except:
            print("error : " + str(hostPort) + " not available")
            #option to try a different port
            tryOpt = raw_input("Try other port (y, n)?")
            if tryOpt == "y":
                hostPort = hostPort + 1
                continue
            elif tryOpt == "n":
                sys.exit()
            else:
                print("error : INVALID PARAMETER " + tryOpt)
                continue
    print("[@] server started at " + hostIP + ":" + str(hostPort))
    #begin listening
    sock.listen(3)
    print("[. . .] waiting for request . . . \n\n")
    #begin loop for accepting clients
    while True:
        #accept client and address
        #open list of restricted ips
        with open(cmdFunctions.resIPfile, "rb") as resIpRBfile:
            resIPlist = load(resIpRBfile)
        #start cursor for sqlite log
        if logConnections:
            cur = aDBtable.cursor()
        #accept client
        (clientSock, addr) = sock.accept()
        cIP = addr[0]
        cPort = str(addr[1])
        #check if restricted ip is wanting to connect
        if cIP in resIPlist:
            print("[=O] found restricted IP trying to connect " + cIP + "; stopping current session\n")
            #record connection into log
            if logConnections:
                cur.execute("""INSERT INTO connections (connDate, ip, authSucceed) VALUES (?, ?, ?)""", (datetime.now(), cIP, "Restricted"))
                aDBtable.commit()
            continue
        print("[+] connected to " + cIP + ":" + cPort)
        #start session
        session = paramiko.Transport(clientSock)
        #add server key to session
        session.add_server_key(hostKey)
        #create server instance
        instServer = Server(importantVars["unames"])
        #get server onto session
        session.start_server(server=instServer)
        #create channel
        chan = session.accept()
        #check if authenticated
        if chan == None:
            print("[>=|] authentincation failed with " + cIP + "!\nadding to restricted ip list\n")
            #save ip to restricted IPs
            resIPlist.append(cIP)
            dump(resIPlist, open(cmdFunctions.resIPfile, "wb"))
            #log the connection
            if logConnections:
                cur.execute("""INSERT INTO connections (connDate, ip, authSucceed) VALUES (?, ?, ?)""", (datetime.now(), cIP, "False"))
                aDBtable.commit()
            continue
        if logConnections:
            #if all is good, log connection as authentication succeeded
            cur.execute("""INSERT INTO connections (connDate, ip, authSucceed) VALUES (?, ?, ?)""", (datetime.now(), cIP, "True"))
            aDBtable.commit()
        print("[<=D] authentication succeeded!")
        print("[*] beginning terminal")
        while True:
            #get command
            aCmd = chan.recv(2000).decode("utf-8")
            print("[==>] received cmd from remote " + cIP)
            print("[cmd] " + aCmd)
            #check if the command is a special command
            #if the command is quit
            if aCmd == "quit":
                print("[-] quitting terminal; exiting session\n")
                if logConnections:
                    cur.execute("""INSERT INTO cmds (cmdData, ip, cmd, out) VALUES (?, ?, ?, ?)""", (datetime.now(), cIP, "quit", "None"))
                    aDBtable.commit()
                #send client quit response
                chan.send("quit")
                #close session
                session.close()
                #exit tertminal loop, but continue accepting clients
                break
            if aCmd == "help":
                if logConnections:
                    cur.execute("""INSERT INTO cmds (cmdData, ip, cmd, out) VALUES (?, ?, ?, ?)""", (datetime.now(), cIP, "help", "None"))
                    aDBtable.commit()
                chan.send(cmdHelpText.encode("utf-8"))
                print("[<==] sent help text to user " + cIP)
                continue
            #if the command causes and error or is restricted by server host
            if aCmd in importantVars["resCmds"]:
                print("terminal error : restricted command " + aCmd)
                if logConnections:
                    cur.execute("""INSERT INTO cmds (cmdData, ip, cmd, out) VALUES (?, ?, ?, ?)""", (datetime.now(), cIP, aCmd, "error"))
                    aDBtable.commit()
                chan.send("error : RESTRICTED COMMAND " + aCmd)
                continue
            #execute the command
            try:
                outs = exeCMD(aCmd, shell=True)
            except Exception as err:
                print("terminal error : invalid/unavailable command " + aCmd)
                if logConnections:
                    cur.execute("""INSERT INTO cmds (cmdData, ip, cmd, out) VALUES (?, ?, ?, ?)""", (datetime.now(), cIP, aCmd, "error"))
                    aDBtable.commit()
                print("[out]")
                print(err)
                chan.send(str(err))
                continue
            #check command output
            if len(outs) == 0:
                outs = "Null"
            #print command in server
            print("[out]")
            print(outs)
            #log the command
            if logConnections:
                cur.execute("""INSERT INTO cmds (cmdData, ip, cmd, out) VALUES (?, ?, ?, ?)""", (datetime.now(), cIP, aCmd, outs))
                aDBtable.commit()
            #send output to client
            chan.send(outs.encode("utf-8"))
            print("[<==] sent output to remote " + cIP)

#execute in linux cmd
def main():
    #credits and usage
    if len(sys.argv[1:]) != 1 and len(sys.argv[1:]) != 2 and len(sys.argv[1:]) != 4 and len(sys.argv[1:]) != 5 and len(sys.argv[1:]) != 6:
        print(helpText)
        #exit to not cause an error
        sys.exit()
    #accept arguments
    #check first parameter
    opt = sys.argv[1]
    #if option is to create password
    #if the user wants to see the help text (same as entering the ./terminal.py command)
    if opt == "help":
        print(helpText)
        sys.exit()
    elif opt == "createPswd":
        #get password and hash it
        pswd2Hash = sys.argv[2]
        cmdFunctions.createStoreHash(pswd2Hash)
    #if option is to reset everything
    elif opt == "reset":
        cmdFunctions.resResIp()
        cmdFunctions.createLogTable
    #if the user wants to remove a restricted ip from the list
    elif opt == "removeResIP":
        ipToRemove = sys.argv[2]
        cmdFunctions.removeAResIP(ipToRemove)
    #if the user wants to add a restricted ip to list
    elif opt == "addResIP":
        ipToAdd = sys.argv[2]
        cmdFunctions.addResIP(ipToAdd)
    #if option is to view data
    elif opt == "viewData":
        #sqlite data
        print("log")
        cmdFunctions.viewSqliteDataLog()
        print("\nrestricted ips")
        cmdFunctions.viewResIpData()
        print("\ncommands")
        cmdFunctions.viewSqliteDataCmd()
    elif opt == "viewClientData":
        cmdFunctions.viewClientData()
    #if the user wants to update to the latest version
    elif opt == "update":
        #get terminal.py
        try:
            terminalFile = getPage("https://raw.githubusercontent.com/bendacoder/pyTerminal/master/terminal.py").read()
        except:
            print("error : CANNOT GET terminal.py")
            sys.exit()
        #get readme.md
        try:
            readmeFile = getPage("https://raw.githubusercontent.com/bendacoder/pyTerminal/master/README.md").read()
        except:
            print("error : CANNOT GET README.md")
            sys.exit()
        #write to files
        open("terminal.py", "w").write(terminalFile)
        open("README.md", "w").write(readmeFile)
    #if option is to execute as client
    elif opt == "client":
        ip = sys.argv[2]
        try:
            port = int(sys.argv[3])
        except:
            print("error : PORT MUST BE INT")
            sys.exit()
        uname = sys.argv[4]
        paswd = sys.argv[5]
        #log connections options
        try:
            logConnec = sys.argv[6]
            if logConnec == "True":
                logConnec = True
            elif logConnec == "False":
                logConnec = False
            else:
                print("error : INVALID PARAMETER " + logConnec + "\nMUST BE BOOL")
                sys.exit()
        except:
            logConnec = False
        getAcTerm(ip, port, uname, paswd, logConnec)
    #if option is to start server
    elif opt == "server":
        #get parameters
        ip = sys.argv[2]
        #if user wants to use local host
        if ip == "local":
            ip = gethostbyname(gethostname())
        port = sys.argv[3]
        try:
            port = int(port)
        except:
            print("error : PORT MUST BE INT")
            sys.exit()
        hostKeyPath = sys.argv[4]
        #add excetions to parameters
        try:
            hostKeyPswd = sys.argv[5]
            if hostKeyPswd == "None":
                hostKeyPswd = None
        except:
            hostKeyPswd = None
        try:
            logConns = sys.argv[6]
            if logConns == "True":
                logConns = True
            elif logConns == "False":
                logConns = False
            else:
                print("error : CANNOT PARSE PARAMETER FOR LOGGING CONNECTIONS " + logConns + "; must be bool; defaulting")
                logConns = False
        except:
            logConns = False
        #start server
        try:
            terminal(ip, port, hostKeyPath, hostKeyPswd, logConns)
        #if user types ^C or del
        except KeyboardInterrupt:
            print("\n[-] exiting terminal")
            sys.exit()
    else:
        print("error : INVALID PARAMETER " + opt)
        sys.exit()

#execute in cmd
main()
